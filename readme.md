![alt text](http://www.evolutionagents.com/images/logo_agents.png "Logo Evolution")

#** TAREAS **
Para ver las tareas pendientes/realizadas hay que mirar en: [Todoist](https://todoist.com/app?lang=en&v=726#project%2F168460673)

#** DOCKER**
##Prerequisitos
* `sudo apt-get install linux-image-extra-$(uname -r)`
* Install docker `curl -fsSL https://get.docker.com/ | sh`
* Start docker daemon `sudo service docker start`
* Verify docker install `sudo docker run hello-world`
* Install docker-compose ```curl -L https://github.com/docker/compose/releases/download/1.6.2/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose```
* Permisos `chmod +x /usr/local/bin/docker-compose`
* Modify group `sudo usermod -aG docker <user_name>`
* Bajar el repositorio
* Install `sudo apt-get install php5-cli`
* Install `sudo curl -sS https://getcomposer.org/installer | php`
* `sudo mv composer.phar /usr/local/bin/composer`
* En la carpeta `evolution`: `composer install`
* Ya podemos hacer build de nuestro docker y runearlo

## Importante
* Compilar imagen `docker-compose build`
* Ejecutar `docker-compose up` (de fondo `-d`)
* Listar dockers `docker ps` (apagados `-a`)
* Logs `docker log <id>`
* Inspeccionar docker `docker exec -t -i <id> sh`
* Entrar a postgres `docker exec -t -i <id postgres> psql -U postgres`
________________________________

#**PLUGINS JAVASCRIPT**
[Plugin para el sliderBar de filters](http://www.jqueryrain.com/?LgYVKmg5)
________________________________

#**ESTRUCTURA SASS**
Para la estrcutura de estilos del proyecto tendremos una carpeta **_modules** donde estaran todos los contenido de modulos tipo navigators, header, footer...
________________________________

#**CREDENCIALES VIRTUAL**
* **username:** univers
* **password:** evo123
________________________________

#**TECNOLOGIAS**
* HTML5
* CSS3
* SCSS
* SASS
* JavaScript
* Algunas librerias de JS
* PHP
* Symfony
________________________________

#**ORGANIZACIÓN BBDD**
> (*) Atributos obligatorio
##User
* (*) ID
* (*) Name
* (*) Username
* (*) Email
* (*) Phone
* (*) Ship name
* (*) Ship role
* (*) Password
* Birthday

##Event
* (*) ID
* (*) Title
* (*) Photo/s
* (*) Description
* Price
* Website
* Event date
* Validity Date
* Location
* (*) Status
* (*) Type
* Category
* Tags
* (*)mapX
* (*)mapY

##TAG
* (*) ID
* (*) Name
________________________________
#**Documentación**
##Explicación aplicativo
[Explicación de la funcionalidad y estructura](https://docs.google.com/document/d/1F8JsQIRBFORG7Jyyp-tS2i4r8YHjO8nWkn1-Gw-hymc/edit# "Title")
##Links
[Campus modulo Proyecto](http://campus.iam.cat/moodle/course/view.php?id=280)
##Documentació drive
[Drive](https://drive.google.com/open?id=0B5MMlGhT2YinTFludnBwQ1c3bFU)
##Diagrama de gantt
[Ver el diagrama online](https://www.smartapp.com/gantterforgoogledrive/index.html?fileID=0B3cJPAbXstQpZmp2QkNBOEtiaXM#)



________________________________

#**METALANGUAGE**
##JavaScript
* Añadir un diccionario de @shortcurts al incio de cada uno de ficheros
* La sintaxi de los shortcurts es tal que: // @nombreShortCurt -> Expli$
> Esto es un exemple de los shortcurts:
>
> // @SearchTags   -> Searcher para buscar tags (función AJAX)
>
> // @LoadPublicEvents -> Carga todos los eventos publicos (función AJAX)
* Jquery: $nombreVariable (Indica que es un objeto jQuery)
> $searcher = $('#searcher');
* String: sNombreVariable (Indica que es de tipo String)
> sUsername = $('#username').val();

________________________________

#**ESTUDI DE MERCAT**
##Producte dirigit a:

La nostra aplicació va dirigida als clients de l'empresa 'Evolution'. Dins d'aquesta, els nostres usuaris podran assistir als diferents events als quals compleixin els requeriments necessaris d'asistència com la restricció d'edat, de lloc i entrades entre altres.

##Inversio econòmica

La inversió inicial del nostre projecte serà nula donat que treballarem en el servidor local on realitzarem tota la funcionalitat de l'aplicació. Posteriorment, a la publicació de l'aplicació serà necessària una inversió aproximada de 150€ anuals per a alquilar un servidor capaç d'emmagatzemar un total de 500 usuari en la primera versió(a debatre amb l'empresa).