<?php
namespace AppBundle\Controller;

use AppBundle\Entity\FileEvo;
use AppBundle\Form\AdminFormType;
use AppBundle\Form\UserFormType;
use AppBundle\Form\EventFormType;
use AppBundle\Form\CategoryFormType;
use AppBundle\Form\TagFormType;
use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Doctrine\ORM\EntityManager;
use AppBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Entity\Admin;
use AppBundle\Entity\Category;
use AppBundle\Entity\Event;
use AppBundle\Entity\Tag;
use AppBundle\Entity\Users;
use AppBundle\Entity\Petition;

class AdminController extends BaseController
{

    /**
     * @Route ("/admin", name="admin_index")
     */
    public function getIndex()
    {
        $em = $this->getDoctrine()->getManager();
        $eventRepository        = $em->getRepository('AppBundle:Event');
        $categoryRepository     = $em->getRepository('AppBundle:Category');
        $tagRepository          = $em->getRepository('AppBundle:Tag');
        $userRepository         = $em->getRepository('AppBundle:Users');
        $petitionRepository     = $em->getRepository('AppBundle:Petition');
        $commentsRepository     = $em->getRepository('AppBundle:Comments');
        $allCategories          = $categoryRepository->findAll();
        $allUsers               = $userRepository->findAll();
        $events                 = $eventRepository->findAll();



        $this->addData('totalTags',          count($tagRepository->findAll()));
        $this->addData('totalCategories',    count($categoryRepository->findAll()));
        $this->addData('totalEvents',        count($eventRepository->findAll()));
        $this->addData('totalEventsFeatured',count($eventRepository->findBy(array('isOutstanding' => true))));

        $this->addData('totalUsers',         count($userRepository->findAll()));
        $this->addData('usersToValidate',    count($userRepository->findBy(array('isActive' => false))));
        $this->addData('totalValorations',   count($commentsRepository->findAll()));
        $this->addData('petitionsToCheck',   count($petitionRepository->findBy(array('isChecked' => false))));


        // Ultimos 5 tags añadidos
        $this->addData('lastTagsAdded', $tagRepository->findBy(array(), array('id' => 'DESC'), 5));


        // TOP10 de categorias (Las más pobladas)
        foreach($allCategories as $category)
            $topCategories[$category->getName()] = count($eventRepository->findBy(array('category' => $category)));

        arsort($topCategories);
        $top5Categories = array_slice($topCategories, 0, 5);

        $this->addData('top5Categories', $top5Categories);


        // TOP10 de usuarios más consumidores
        foreach($allUsers as $user)
            $topUsers[$user->getUsername()] = count($user->getHistoric());

        arsort($topUsers);
        $top10Users = array_slice($topUsers, 0, 5);

        $this->addData('top5Users', $top10Users);

        //añadimos los eventos mas comprados ordenados si hay historico
        if(count($em->getRepository('AppBundle:Historic')->findAll())>0){
            $this->addData('eventsMostPurchased', $this->getEventsMostPurchased($events, $em));
        }else{
            $this->addData('eventsMostPurchased', null);
        }


        // TOP5 Últimos comentarios/valoraciones
        $this->addData('lastComments', $commentsRepository->findBy(array(), array('id' => 'DESC'), 5));
        $this->checkeUsersValidate();
        $this->checkPetitions();
        return $this->render('AppBundle:admin:index/index.html.twig', $this->getData());
    }

    private function getEventsMostPurchased($events, $em){

        $eventsMostPursached = array();
        //introducimos en el array $eventsMostPursached el porcentage de compra del evento relativo a todas las compras de la aplicacion la key es la id del evento y el value es el porcentage de compra
        foreach($events as $event){
            $eventsMostPursached[$event->getId()] = (count($em->getRepository('AppBundle:Historic')->findBy(array('event'=>$event->getId())))/count($em->getRepository('AppBundle:Historic')->findAll()))*100;
        }

        //ordenamos el array de mayor a menor
        arsort($eventsMostPursached);

        //cogemos los 10 mas comprados de toda la aplicacion correspondiente a los tags del usuario y en caso de tener categoria a la categoria tambien
        $eventsMostPursached = array_slice($eventsMostPursached, 0, 5, true);

        $eventsMostPursachedOrder = array();

        //cogemos los eventos
        foreach($eventsMostPursached as $eventId => $value){
            $eventsMostPursachedOrder[] = $em->getRepository('AppBundle:Event')->find($eventId);
        }

        return $eventsMostPursachedOrder;
    }


    /**
     * @Route ("/admin/insert/user", name="create_user")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function createUser(Request $request)
    {

        // Se genera el formulario de datos

        $user = new Users();

        $em = $this->getDoctrine()->getManager();

        $em->persist($user);

        $form = $this->createForm(UserFormType::class, $user);

        // Realizamos el hadle de los datos del formulario
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {

            $password = $this->get('security.password_encoder')
                ->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);

            $user->setIsActive(true);

            $em = $this->getDoctrine()->getManager();
            $em->flush();

            return $this->redirectToRoute('create_user');

        }

        $this->addData('edit', false);
        $this->addData('formUser', $form->createView());
        $this->checkeUsersValidate();
        $this->checkPetitions();
        return $this->render('AppBundle:admin:user/createUser.html.twig', $this->getData());
    }

    /**
     * @Route ("/admin/edit/user/{id}", name="edit_user")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function editUser(Request $request, $id)
    {


        $em = $this->getDoctrine()->getManager();

        $user = $em->getRepository('AppBundle:Users')->find($id);

        if(!$user instanceof Users){
            $message = "ERROR incorrect user";
            print_r($message);
            die();
        }
        $form = $this->createForm(UserFormType::class, $user)->remove('plainPassword');

        // Realizamos el hadle de los datos del formulario
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->flush();

            return $this->redirectToRoute('list_user');

        }

        $this->addData('edit', true);
        $this->addData('formUser', $form->createView());
        $this->checkeUsersValidate();
        $this->checkPetitions();
        return $this->render('AppBundle:admin:user/createUser.html.twig', $this->getData());
    }



    /**
     * @Route ("/admin/delete/user", name="delete_user")
     */
    public function deleteUserAction(Request $request)
    {

        $id = $request->request->get('id');


        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('AppBundle:Users')->find($id);

        if (empty($user)) {
            $this->sendResponseStatus('ERROR');

            return new JSONResponse($this->getData());
        }

        $em->remove($user);
        $em->flush();

        $this->sendResponseStatus('OK');



        // Generamos los datos para la respuesta ajax
        return new JSONResponse($this->getData());
    }


    /**
     * @Route ("/admin/list/user", name="list_user")
     */
    public function listUser()
    {
        $em = $this->getDoctrine()->getManager();

        $allUsersActive = $em->getRepository('AppBundle:Users')->findBy(array("isActive" => true));

        $this->addData('users', $allUsersActive);
        $this->checkeUsersValidate();
        $this->checkPetitions();
        return $this->render('AppBundle:admin:user/listUser.html.twig', $this->getData());
    }


    /**
     * @Route("/admin/insert/admin", name="create_admin")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function createAdmin(Request $request)
    {

        // Se genera el formulario de datos

        $admin = new Admin();

        $em = $this->getDoctrine()->getManager();

        $em->persist($admin);

        $form = $this->createForm(AdminFormType::class, $admin);

        // Realizamos el hadle de los datos del formulario
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $password = $this->get('security.password_encoder')
                ->encodePassword($admin, $admin->getPlainPassword());
            $admin->setPassword($password);

            $em = $this->getDoctrine()->getManager();
            $em->flush();

            return $this->redirectToRoute('create_admin');
        }

        $this->addData('formAdmin', $form->createView());
        $this->checkeUsersValidate();
        $this->checkPetitions();

        return $this->render('AppBundle:admin:admin/createAdmin.html.twig', $this->getData());
    }

    /**
     * @Route ("/admin/edit/admin/{id}", name="edit_admin")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function editAdmin(Request $request, $id)
    {


        $em = $this->getDoctrine()->getManager();

        $admin = $em->getRepository('AppBundle:Admin')->find($id);

        if(!$admin instanceof Admin){
            $message = "ERROR incorrect admin";
            print_r($message);
            die();
        }
        $form = $this->createForm(AdminFormType::class, $admin)->remove('plainPassword');

        // Realizamos el hadle de los datos del formulario
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->flush();

            return $this->redirectToRoute('list_admin');

        }

        $this->addData('edit', true);
        $this->addData('formAdmin', $form->createView());
        $this->checkeUsersValidate();
        $this->checkPetitions();

        return $this->render('AppBundle:admin:admin/createAdmin.html.twig', $this->getData());
    }

    /**
     * @Route ("/admin/delete/admin", name="delete_admin")
     */
    public function deleteAdminAction(Request $request)
    {

        $id = $request->request->get('id');

        $em = $this->getDoctrine()->getManager();
        $admin = $em->getRepository('AppBundle:Admin')->find($id);

        if (empty($admin)) {
            $this->sendResponseStatus('ERROR');

            return new JSONResponse($this->getData());
        }

        $em->remove($admin);
        $em->flush();

        $this->sendResponseStatus('OK');


        // Generamos los datos para la respuesta ajax
        return new JSONResponse($this->getData());
    }


    /**
     * @Route ("/admin/list/admin", name="list_admin")
     */
    public function listAdmin()
    {
        $em = $this->getDoctrine()->getManager();

        $allAdmins = $em->getRepository('AppBundle:Admin')->findAll();
        $this->addData('admins', $allAdmins);
        $this->checkeUsersValidate();
        $this->checkPetitions();

        return $this->render('AppBundle:admin:admin/listAdmin.html.twig', $this->getData());
    }


    /**
     * @Route("/admin/insert/event", name="create_event")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function addEventAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $event = new Event();

        $form = $this->createForm(EventFormType::class, $event);
        // Realizamos el hadle de los datos del formulario
        $form->handleRequest($request);

        //comprovacion de ficheros
        if ($form->isSubmitted() && $form->isValid()) {
            if ($event->getDate() >= $event->getValidDate()) {
                $session = $request->getSession();

                $files = $session->get('filesIDs');
                for ($i=0;$i<$session->get('countFiles');$i++) {
                    $file = $this->getDoctrine()
                                ->getRepository('AppBundle:FileEvo')
                                ->find($files[$i]);
                    $file->setEvent($event);
                    $em->persist($file);
                    $em->flush();
                }

                $em->persist($event);
                $em->flush();
                $session->set('filesIDs',null);
                $session->set('countFiles',0);
                return $this->redirectToRoute('create_event');
            } else {
                $message = "La fecha de realización es incorrecto";
                print_r($message);
                die();
            }
        }
        $this->addData('edit', false);
        $this->addData('formEvent', $form->createView());
        $this->addData('mapX', $event->getMapX());
        $this->addData('mapY', $event->getMapY());
        $this->checkeUsersValidate();
        $this->checkPetitions();

        return $this->render('AppBundle:admin:event/createEvent.html.twig', $this->getData());
    }

    /**
     * @Route("/admin/edit/event/{id}", name="edit_event")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function editEventAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $event = $em->getRepository('AppBundle:Event')->find($id);

        if(!$event instanceof Event){
            $message = "ERROR incorrect event";
            print_r($message);
            die();
        }

        //cogemos las imagenes para añadirlas al template
        $files = $event->getFiles();
        $this->addData('files', $files);


        $form = $this->createForm(EventFormType::class, $event);
        // Realizamos el hadle de los datos del formulario
        $form->handleRequest($request);

        //comprovacion de ficheros
        if ($form->isSubmitted() && $form->isValid()) {
            if ($event->getDate() >= $event->getValidDate()) {
                $session = $request->getSession();

                $files = $session->get('filesIDs');
                for ($i=0;$i<$session->get('countFiles');$i++) {
                    $file = $this->getDoctrine()
                        ->getRepository('AppBundle:FileEvo')
                        ->find($files[$i]);
                    $file->setEvent($event);
                    $em->persist($file);
                    $em->flush();
                }

                $em->persist($event);
                $em->flush();
                $session->set('filesIDs',null);
                $session->set('countFiles',0);
                return $this->redirectToRoute('list_event');
            } else {
                $message = "La fecha de realización es incorrecto";
                print_r($message);
                die();
            }
        }
        $this->addData('edit', true);
        $this->addData('formEvent', $form->createView());
        $this->addData('mapX', $event->getMapX());
        $this->addData('mapY', $event->getMapY());
        $this->checkeUsersValidate();
        $this->checkPetitions();

        return $this->render('AppBundle:admin:event/createEvent.html.twig', $this->getData());
    }

    /**
     * @Route ("/admin/delete/event/file", name="delete_event_file")
     */
    public function deleteFileEvent(Request $request)
    {

        $id = $request->request->get('id');


        $em = $this->getDoctrine()->getManager();
        $file = $em->getRepository('AppBundle:FileEvo')->find($id);

        if (empty($file)) {
            $this->sendResponseStatus('ERROR');

            return new JSONResponse($this->getData());
        }

        $em->remove($file);
        $em->flush();

        $this->sendResponseStatus('OK');

        // Generamos los datos para la respuesta ajax
        return new JSONResponse($this->getData());
    }


    /**
     * @Route ("/admin/delete/event", name="delete_event")
     */
    public function deleteEvent(Request $request)
    {

        $id = $request->request->get('id');


        $em = $this->getDoctrine()->getManager();
        $event = $em->getRepository('AppBundle:Event')->find($id);

        if (empty($event)) {
            $this->sendResponseStatus('ERROR');

            return new JSONResponse($this->getData());
        }

        $em->remove($event);
        $em->flush();

        $this->sendResponseStatus('OK');

        // Generamos los datos para la respuesta ajax
        return new JSONResponse($this->getData());
    }

    /**
     * @Route ("/admin/list/event", name="list_event")
     */
    public function listEvent()
    {
        $em = $this->getDoctrine()->getManager();

        $allEvents = $em->getRepository('AppBundle:Event')->findAll();
        $this->addData('events', $allEvents);
        $this->checkeUsersValidate();
        $this->checkPetitions();

        return $this->render('AppBundle:admin:event/listEvent.html.twig', $this->getData());
    }

    /**
     * @Route ("/admin/history/event", name="history_event")
     * @param Request $request
     * @param $id
     * @return Response
     */
    public function eventHistory()
    {
        $em = $this->getDoctrine()->getManager();

        $eventHistory = $em->getRepository('AppBundle:Historic')->findAll();

        $this->addData('eventHistory', $eventHistory);
        $this->checkeUsersValidate();
        $this->checkPetitions();

        return $this->render('AppBundle:admin:event/eventsHistory.html.twig', $this->getData());
    }


    /**
     * @Route ("/admin/insert/category", name="create_category")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function createCategory(Request $request)
    {
        $category = new Category();
        $em = $this->getDoctrine()->getManager();

        $em->persist($category);

        $form = $this->createForm(CategoryFormType::class, $category);

        // Realizamos el handle de los datos del formulario
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->flush();
            return $this->redirectToRoute('create_category');
        }
        $this->addData('formCategory', $form->createView());
        $this->checkeUsersValidate();
        $this->checkPetitions();

        return $this->render('AppBundle:admin:category/createCategory.html.twig', $this->getData());
    }

    /**
     * @Route ("/admin/edit/category/{id}", name="edit_Category")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function editCategory(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $category = $em->getRepository('AppBundle:Category')->find($id);

        if(!$category instanceof Category){
            $message = "ERROR incorrect category";
            print_r($message);
            die();
        }
        $form = $this->createForm(CategoryFormType::class, $category);

        // Realizamos el hadle de los datos del formulario
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {

            if($category->getIsActive() == false){
                $subcategories = $category->getChildrens();
                foreach($subcategories as $subcategory){
                    $subcategory->setIsActive(false);
                }
            }

            $em = $this->getDoctrine()->getManager();
            $em->flush();

            return $this->redirectToRoute('list_category');

        }

        $this->addData('edit', true);
        $this->addData('formCategory', $form->createView());
        $this->checkeUsersValidate();
        $this->checkPetitions();

        return $this->render('AppBundle:admin:category/createCategory.html.twig', $this->getData());
    }

    /**
     * @Route ("/admin/delete/category", name="delete_category")
     */
    public function deleteCategory(Request $request)
    {
        $id = $request->request->get('id');


        $em = $this->getDoctrine()->getManager();
        $category = $em->getRepository('AppBundle:Category')->find($id);

        if (empty($category)) {
            $this->sendResponseStatus('ERROR');

            return new JSONResponse($this->getData());
        }

        $em->remove($category);
        $em->flush();

        $this->sendResponseStatus('OK');

        // Generamos los datos para la respuesta ajax
        return new JSONResponse($this->getData());
    }

    /**
     * @Route ("/admin/list/category", name="list_category")
     */
    public function listCategory()
    {
        $em = $this->getDoctrine()->getManager();

        $allCategories = $em->getRepository('AppBundle:Category')->findAll();
        $this->addData('categories', $allCategories);
        $this->checkeUsersValidate();
        $this->checkPetitions();

        return $this->render('AppBundle:admin:category/listCategory.html.twig', $this->getData());
    }





    /**
     * @Route("/admin/insert/tag", name="create_tag")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function addTagAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $tag = new Tag();
        $em->persist($tag);
        $form = $this->createForm(TagFormType::class, $tag);

        // Realizamos el hadle de los datos del formulario
        $form->handleRequest($request);

        //comprovacion de ficheros
        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();
            return $this->redirectToRoute('create_tag');
        }

        $this->addData('formTag', $form->createView());
        $this->checkeUsersValidate();
        $this->checkPetitions();

        return $this->render('AppBundle:admin:tag/createTag.html.twig', $this->getData());
    }

    /**
     * @Route ("/admin/edit/tag/{id}", name="edit_tag")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function editTag(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $tag = $em->getRepository('AppBundle:Tag')->find($id);

        if(!$tag instanceof Tag){
            $message = "ERROR incorrect tag";
            print_r($message);
            die();
        }
        $form = $this->createForm(TagFormType::class, $tag);

        // Realizamos el hadle de los datos del formulario
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->flush();

            return $this->redirectToRoute('list_tag');

        }

        $this->addData('edit', true);
        $this->addData('formTag', $form->createView());
        $this->checkeUsersValidate();
        $this->checkPetitions();

        return $this->render('AppBundle:admin:tag/createTag.html.twig', $this->getData());
    }


    /**
     * @Route("/admin/delete/tag", name="delete_tag")
     */
    public function deleteTagAction(Request $request)
    {

        $id     = $request->request->get('id');
        $em     = $this->getDoctrine()->getManager();
        $tag    = $em->getRepository('AppBundle:Tag')->find($id);

        if (empty($tag)) {
            $this->sendResponseStatus('ERROR');

            return new JSONResponse($this->getData());
        }

        $em->remove($tag);
        $em->flush();

        $this->sendResponseStatus('OK');

        // Generamos los datos para la respuesta ajax
        return new JSONResponse($this->getData());
    }

    /**
     * @Route("/admin/fast/active/category", name="fast_active_category")
     */
    public function fastActiveCategoryAction(Request $request)
    {

        $id     = $request->request->get('id');
        $em     = $this->getDoctrine()->getManager();
        $category    = $em->getRepository('AppBundle:Category')->find($id);

        if (empty($category)) {
            $this->sendResponseStatus('ERROR');

            return new JSONResponse($this->getData());
        }

        $category->setIsActive(true);

        $em->flush();

        $this->sendResponseStatus('OK');

        // Generamos los datos para la respuesta ajax
        return new JSONResponse($this->getData());
    }

    /**
     * @Route("/admin/fast/inactive/category", name="fast_inactive_category")
     */
    public function fastInactiveCategoryAction(Request $request)
    {

        $id     = $request->request->get('id');
        $em     = $this->getDoctrine()->getManager();
        $category    = $em->getRepository('AppBundle:Category')->find($id);

        if (empty($category)) {
            $this->sendResponseStatus('ERROR');

            return new JSONResponse($this->getData());
        }

        $category->setIsActive(false);

        $subcategories = $category->getChildrens();
        foreach($subcategories as $subcategory){
            $subcategory->setIsActive(false);
        }
        $em->flush();

        $this->sendResponseStatus('OK');

        // Generamos los datos para la respuesta ajax
        return new JSONResponse($this->getData());
    }

    /**
     * @Route("/admin/fast/public/category", name="fast_public_category")
     */
    public function fastPublicCategoryAction(Request $request)
    {

        $id     = $request->request->get('id');
        $em     = $this->getDoctrine()->getManager();
        $category    = $em->getRepository('AppBundle:Category')->find($id);

        if (empty($category)) {
            $this->sendResponseStatus('ERROR');

            return new JSONResponse($this->getData());
        }

        $category->setIsPublic(true);

        $em->flush();

        $this->sendResponseStatus('OK');

        // Generamos los datos para la respuesta ajax
        return new JSONResponse($this->getData());
    }

    /**
     * @Route("/admin/fast/private/category", name="fast_private_category")
     */
    public function fastPrivateCategoryAction(Request $request)
    {

        $id     = $request->request->get('id');
        $em     = $this->getDoctrine()->getManager();
        $category    = $em->getRepository('AppBundle:Category')->find($id);

        if (empty($category)) {
            $this->sendResponseStatus('ERROR');

            return new JSONResponse($this->getData());
        }

        $category->setIsPublic(false);

        $subcategories = $category->getChildrens();
        foreach($subcategories as $subcategory){
            $subcategory->setIsPublic(false);
        }
        $em->flush();

        $this->sendResponseStatus('OK');

        // Generamos los datos para la respuesta ajax
        return new JSONResponse($this->getData());
    }

    /**
     * @Route("/admin/fast/active/event", name="fast_active_event")
     */
    public function fastActiveEventAction(Request $request)
    {

        $id     = $request->request->get('id');
        $em     = $this->getDoctrine()->getManager();
        $event    = $em->getRepository('AppBundle:Event')->find($id);

        if (empty($event)) {
            $this->sendResponseStatus('ERROR');

            return new JSONResponse($this->getData());
        }

        $event->setIsActive(true);

        $em->flush();

        $this->sendResponseStatus('OK');

        // Generamos los datos para la respuesta ajax
        return new JSONResponse($this->getData());
    }

    /**
     * @Route("/admin/fast/inactive/event", name="fast_inactive_event")
     */
    public function fastInactiveEventAction(Request $request)
    {

        $id     = $request->request->get('id');
        $em     = $this->getDoctrine()->getManager();
        $event    = $em->getRepository('AppBundle:Event')->find($id);

        if (empty($event)) {
            $this->sendResponseStatus('ERROR');

            return new JSONResponse($this->getData());
        }

        $event->setIsActive(false);

        $em->flush();

        $this->sendResponseStatus('OK');

        // Generamos los datos para la respuesta ajax
        return new JSONResponse($this->getData());
    }

    /**
     * @Route("/admin/fast/public/event", name="fast_public_event")
     */
    public function fastPublicEventAction(Request $request)
    {

        $id     = $request->request->get('id');
        $em     = $this->getDoctrine()->getManager();
        $event    = $em->getRepository('AppBundle:Event')->find($id);

        if (empty($event)) {
            $this->sendResponseStatus('ERROR');

            return new JSONResponse($this->getData());
        }

        $event->setIsPublic(true);

        $em->flush();

        $this->sendResponseStatus('OK');

        // Generamos los datos para la respuesta ajax
        return new JSONResponse($this->getData());
    }

    /**
     * @Route("/admin/fast/private/event", name="fast_private_event")
     */
    public function fastPrivateEventAction(Request $request)
    {

        $id     = $request->request->get('id');
        $em     = $this->getDoctrine()->getManager();
        $event    = $em->getRepository('AppBundle:Event')->find($id);

        if (empty($event)) {
            $this->sendResponseStatus('ERROR');

            return new JSONResponse($this->getData());
        }

        $event->setIsPublic(false);

        $em->flush();

        $this->sendResponseStatus('OK');

        // Generamos los datos para la respuesta ajax
        return new JSONResponse($this->getData());
    }


    /**
     * @Route ("/admin/list/tag", name="list_tag")
     */
    public function listTag()
    {
        $em      = $this->getDoctrine()->getManager();
        $allTags = $em->getRepository('AppBundle:Tag')->findAll();

        $this->addData('tags', $allTags);
        $this->checkeUsersValidate();
        $this->checkPetitions();

        return $this->render('AppBundle:admin:tag/listTag.html.twig', $this->getData());
    }


    /**
     * @Route ("/admin/validate/list", name="validate_list")
     */
    public function ValidateUserView()
    {
        $em = $this->getDoctrine()->getManager();
        $usersToValidate = $em->getRepository('AppBundle:Users')->findBy(array('isActive' => false));

        $this->addData('users', $usersToValidate);
        $this->checkeUsersValidate();
        $this->checkPetitions();

        return $this->render('AppBundle:admin:user/validateUser.html.twig', $this->getData());

    }

    /**
     * @Route ("/admin/validate/user/{id}", name="validate_user")
     */
    public function ValidateUserAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $em->getRepository('AppBundle:Users')->find($id);
        $user->setIsActive(true);

        $em->persist($user);
        $em->flush();

        return $this->redirect("/admin/validate/list");
    }

    /**
     * @Route ("/admin/validate/cancel/{id}", name="cancel_validate_user")
     */
    public function cancelValidateUserAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $em->getRepository('AppBundle:Users')->find($id);
        $em->remove($user);
        $em->flush();

        return $this->redirect("/admin/validate/list");
    }

    /**
     * @Route ("/admin/petitions", name="list_petitions")
     */
    public function listPetitions()
    {
        $em = $this->getDoctrine()->getManager();
        $petitions = $em->getRepository('AppBundle:Petition')->findBy(array('isChecked' => false));
        $this->addData('petitions', $petitions);
        $this->checkeUsersValidate();
        $this->checkPetitions();

        return $this->render('AppBundle:admin:petition/listPetitions.html.twig', $this->getData());
    }

    /**
     * @Route ("/admin/checkPetition/{id}", name="check_petition")
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function checkPetition(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $petition   = $em->getRepository('AppBundle:Petition')->find($id);
        $admin      = $this->get('security.token_storage')->getToken()->getUser();
        $petition->setIsChecked(true);
        $petition->setValidatedBy($admin);
        $petition->setValidationDate(new \DateTime());

        $em->persist($petition);
        $em->flush();

        return $this->redirect("/admin/petitions");
    }


    /**
     * @Route ("/admin/history/petitions", name="history_petitions")
     * @param Request $request
     * @param $id
     * @return Response
     */
    public function petitionHistory()
    {
        $em = $this->getDoctrine()->getManager();

        $petitionsHistory = $em->getRepository('AppBundle:Petition')->findBy(array('isChecked' => true));

        $this->addData('petitionsHistory', $petitionsHistory);
        $this->checkeUsersValidate();
        $this->checkPetitions();

        return $this->render('AppBundle:admin:petition/petitionsHistory.html.twig', $this->getData());
    }

    private function checkeUsersValidate(){
        $em = $this->getDoctrine()->getManager();

        if(count($em->getRepository('AppBundle:Users')->findBy(array('isActive' => false)))>0){
            $this->addData('checkUsersToValidate', true);
        }else{
            $this->addData('checkUsersToValidate', false);
        }

    }

    private function checkPetitions(){
        $em = $this->getDoctrine()->getManager();

        if(count($em->getRepository('AppBundle:Petition')->findBy(array('isChecked' => false)))>0){
            $this->addData('checkPetitionsPending', true);
        }else{
            $this->addData('checkPetitionsPending', false);
        }

    }

}