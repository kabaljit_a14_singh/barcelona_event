<?php
namespace AppBundle\Controller;

use AppBundle\Entity\Category;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Doctrine\ORM\EntityManager;
use AppBundle\Controller\BaseController;

class CategoryController extends BaseController{

    /**
     * @Route ("/category/{id}", name="category")
     */
    public function getCategory($id){

        $em = $this->getDoctrine()->getManager();

        $category = $em->getRepository('AppBundle:Category')->find($id);

        if((!$category instanceof Category)){
            $message="Categoria incorrecta";
            print_r($message);
            die();
        }

        $this->puedeVer($category);

        //comprobamos que no es una subcategoria, en caso de serlo, lo redirigimos a la vista de subcategorias
        if($category->getParentCategory() != null){
            return $this->redirectToRoute('subcategory', array('id'=>$id));
        }

        $subcategories = array();

        //comprobamos si esta logeado o no
        $securityContext = $this->container->get('security.authorization_checker');
        if ($securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            // authenticated REMEMBERED, FULLY will imply REMEMBERED (NON anonymous)

            $subcategoriesRepo = $em->getRepository('AppBundle:Category')->findBy(array('parentCategory'=>$id, 'isActive'=>true));

            foreach($subcategoriesRepo as $sub){
                $subcategories[] = array('subcategory'=>$sub, 'count'=>count($em->getRepository('AppBundle:Event')->findBy(array('category'=>$sub->getId(), 'isActive'=>true))));
            }

            $destacados = $em->getRepository('AppBundle:Event')->findBy(array('isOutstanding'=>true, 'category'=>$id, 'isActive'=>true), array('id' => 'DESC'), 4);

            if(count($destacados)<4){
                foreach($subcategoriesRepo as $sub){
                    if(count($destacados)<4){
                        $destacadosSubcategory = $em->getRepository('AppBundle:Event')->findBy(array('isOutstanding'=>true, 'category'=>$sub->getId(), 'isActive'=>true), array('id' => 'DESC'), 4 - count($destacados));
                        foreach($destacadosSubcategory as $desc){
                            $destacados[] = $desc;
                        }
                    }else{
                        break;
                    }
                }
            }
            $ultimos = $em->getRepository('AppBundle:Event')->findBy(array('category'=>$id, 'isActive'=>true), array('id' => 'DESC'), 4);

            if($securityContext->isGranted('ROLE_USER')){
                $user = $this->get('security.token_storage')->getToken()->getUser();
                $recomendados = $this->getRecomended($user, $category);
                $this->addData('recomendados', $recomendados);
            }


        }else{
            $subcategoriesRepo = $em->getRepository('AppBundle:Category')->findBy(array('parentCategory'=>$id, 'isPublic' => true, 'isActive'=>true));

            foreach($subcategoriesRepo as $sub){
                $subcategories[] = array('subcategory'=>$sub, 'count'=>count($em->getRepository('AppBundle:Event')->findBy(array('category'=>$sub->getId(), 'isPublic' => true, 'isActive'=>true))));
            }

            $destacados = $em->getRepository('AppBundle:Event')->findBy(array('isOutstanding'=>true, 'isPublic' => true, 'category'=>$id, 'isActive'=>true), array('id' => 'DESC'), 4);

            if(count($destacados)<4){
                foreach($subcategoriesRepo as $sub){
                    if(count($destacados)<4){
                        $destacadosSubcategory = $em->getRepository('AppBundle:Event')->findBy(array('isOutstanding'=>true, 'isPublic' => true, 'category'=>$sub->getId(), 'isActive'=>true), array('id' => 'DESC'), 4 - count($destacados));
                        foreach($destacadosSubcategory as $desc){
                            $destacados[] = $desc;
                        }
                    }else{
                        break;
                    }
                }
            }
            $ultimos = $em->getRepository('AppBundle:Event')->findBy(array('isPublic' => true, 'category'=>$id, 'isActive'=>true), array('id' => 'DESC'), 4);
        }

        $this->addData('subcategories', $subcategories);
        $this->addData('destacados', $destacados);
        $this->addData('ultimos', $ultimos);
        $this->addData('titulo', 'Category page');
        return $this->render('AppBundle:category:category.html.twig', $this->getData());
    }


    /**
     * @Route ("/subcategory/{id}", name="subcategory")
     */
    public function getSubcategory($id){
        $em = $this->getDoctrine()->getManager();

        $category = $em->getRepository('AppBundle:Category')->find($id);

        if(!$category instanceof Category){
            $message="Categoria incorrecta";
            print_r($message);
            die();
        }

        $this->puedeVer($category);

        //comprobamos que no es una categoria padre, en caso de serlo, lo redirigimos a la vista de categorias
        if($category->getParentCategory() == null){
            return $this->redirectToRoute('category', array('id'=>$id));
        }


        $padre = $category->getParentCategory()->getId();

        $subcategories = array();

        //comprobamos si esta logeado o no
        $securityContext = $this->container->get('security.authorization_checker');
        if ($securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            // authenticated REMEMBERED, FULLY will imply REMEMBERED (NON anonymous)

            $subcategoriesRepo = $em->getRepository('AppBundle:Category')->findBy(array('parentCategory'=>$padre, 'isActive'=>true));

            foreach($subcategoriesRepo as $sub){
                $subcategories[] = array('subcategory'=>$sub, 'count'=>count($em->getRepository('AppBundle:Event')->findBy(array('category'=>$sub->getId(), 'isActive'=>true))));
            }

            $events = $em->getRepository('AppBundle:Event')->findBy(array('category'=>$id, 'isActive'=>true), array('id' => 'DESC'));

            if($securityContext->isGranted('ROLE_USER')){
                $user = $this->get('security.token_storage')->getToken()->getUser();
                $recomendados = $this->getRecomended($user, $category);
                $this->addData('recomendados', $recomendados);
            }

        }else{
            $subcategoriesRepo = $em->getRepository('AppBundle:Category')->findBy(array('parentCategory'=>$padre, 'isPublic' => true, 'isActive'=>true));

            foreach($subcategoriesRepo as $sub){
                $subcategories[] = array('subcategory'=>$sub, 'count'=>count($em->getRepository('AppBundle:Event')->findBy(array('category'=>$sub->getId(), 'isPublic' => true, 'isActive'=>true))));
            }

            $events = $em->getRepository('AppBundle:Event')->findBy(array('isPublic' => true, 'category'=>$id, 'isActive'=>true), array('id' => 'DESC'));

        }
        $this->addData('subcategories', $subcategories);
        $this->addData('events', $events);
        $this->addData('titulo', 'SubCategory page');
        return $this->render('AppBundle:category:subcategory.html.twig', $this->getData());
    }


    public function renderMenuAction()
    {
        $em = $this->getDoctrine()->getManager();


        //comprobamos si esta logeado o no
        $securityContext = $this->container->get('security.authorization_checker');
        if ($securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            // authenticated REMEMBERED, FULLY will imply REMEMBERED (NON anonymous)

            $categoriesDynamic = $em->getRepository('AppBundle:Category')->findBy(array('isActive'=>true));

            $count = count($em->getRepository('AppBundle:Category')->findBy(array('parentCategory'=>null, 'isActive'=>true)));

        }else{
            $categoriesDynamic = $em->getRepository('AppBundle:Category')->findBy(array('isPublic' => true, 'isActive'=>true));

            $count = count($em->getRepository('AppBundle:Category')->findBy(array('parentCategory'=>null, 'isPublic' => true, 'isActive'=>true)));
        }

        $this->addData('countCategories', $count);
        $this->addData('categories', $categoriesDynamic);
        return $this->render('AppBundle:menu:menuCategories.html.twig', $this->getData());
    }

    public function puedeVer(Category $category){
        if(!$category->getIsPublic()){
            $securityContext = $this->container->get('security.authorization_checker');
            if (!$securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
                $message="Categoria privada, registrate para ver esta categoria";
                print_r($message);
                die();
            }
        }
    }
}