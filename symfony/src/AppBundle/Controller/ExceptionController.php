<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Doctrine\ORM\EntityManager;
use AppBundle\Controller\BaseController;


class ExceptionController extends BaseController{

    /**
     * La imagen será distinta para cada uno de los errores como tambien el mensaje
     * @Route ("/error403", name="error403")
     */
    public function showError403Action(){
        $this->addData('image', '/img/error/error403.png');
        $this->addData('error', 'str(you sailed too far');
        return $this->render('AppBundle:exception:error.html.twig', $this->getData());
    }

    /**
     * @Route ("/error404", name="error404")
     */
    public function showError404Action(){
        $this->addData('image', '/img/error/error404.png');
        $this->addData('error', 'str(you turned the wheel too much');
        return $this->render('AppBundle:exception:error.html.twig', $this->getData());
    }

    /**
     * @Route ("/error500", name="error500")
     */
    public function showError500Action(){
        $this->addData('error', 'str(you turned the wheel too much');
        return $this->render('AppBundle:exception:error.html.twig', $this->getData());
    }
}