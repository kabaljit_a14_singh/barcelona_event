<?php
namespace AppBundle\Controller;


use AppBundle\Entity\Admin;
use AppBundle\Entity\Users;
use AppBundle\Form\AdminFormType;
use AppBundle\Form\UserFormType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\JsonResponse;

class SecurityController extends BaseController
{


    /**
     * @Route("/login", name="login_user")
     */

    public function showLogin(Request $request){

        $authenticationUtils = $this->get('security.authentication_utils');

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        $this->addData('titulo', 'Pagina de login de usuarios');
        $this->addData('last_username', $lastUsername);
        $this->addData('error', $error);
        return $this->render('AppBundle:test:loginUser.html.twig', $this->getData());
    }

    /**
     * @Route("/signup", name="signup")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function guardarUser(Request $request)
    {

        // Se genera el formulario de datos

        $user = new Users();

        $em = $this->getDoctrine()->getManager();

        $em->persist($user);

        $form = $this->createForm(UserFormType::class, $user);

        // Realizamos el hadle de los datos del formulario
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {

            $password = $this->get('security.password_encoder')
                ->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);

            $em = $this->getDoctrine()->getManager();
            $em->flush();

            return $this->redirectToRoute('signup');
        }
        $this->addData('formUser', $form->createView());

        return $this->render('AppBundle:signup:signup.html.twig',$this->getData());
    }

    /**
     * @Route("/insert/admin", name="insert_admin")
     *
     * @param Request $request
     *
     * @return Response
     *
    public function guardarAdmin(Request $request)
    {

        // Se genera el formulario de datos

        $admin = new Admin();

        $em = $this->getDoctrine()->getManager();

        $em->persist($admin);

        $form = $this->createForm(AdminFormType::class, $admin);

        // Realizamos el hadle de los datos del formulario
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {

            $password = $this->get('security.password_encoder')
                ->encodePassword($admin, $admin->getPlainPassword());
            $admin->setPassword($password);

            $em = $this->getDoctrine()->getManager();
            $em->flush();

            return $this->redirectToRoute('insert_admin');
        }

        $this->addData('formAdmin', $form->createView());

        return $this->render('AppBundle:test:registerAdmin.html.twig',$this->getData());
    }*/
}