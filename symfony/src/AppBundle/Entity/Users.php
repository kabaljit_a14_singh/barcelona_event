<?php
// src/AppBundle/Entity/Users.php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * @ORM\Entity
 * @ORM\Table(name="users")
 * @ORM\Entity(repositoryClass="UsersRepository")
 * @UniqueEntity("userName")
 * @UniqueEntity("email")
 */
class Users implements AdvancedUserInterface, \Serializable
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     */

    private $surname;

    /**
     * @ORM\Column(type="text", length=255, unique=true)
     * @Assert\NotBlank()
     */

    private $userName;

    /**
     * @ORM\Column(type="text", length=255, unique=true)
     * @Assert\NotBlank()
     * @Assert\Email()
     */

    private $email;

    /**
     *
     * @Assert\Length(max=4096)
     */
    private $plainPassword;

    /**
     *
     * @ORM\Column(type="string", length=64)
     */
    private $password;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     */

    private $shipName;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     */

    private $shipRank;
    /**
     * @ORM\ManyToMany(targetEntity="Tag")
     * @ORM\JoinTable(name="users_tags",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="tag_id", referencedColumnName="id")}
     *      )
     */
    private $tags;

    /**
     * @ORM\OneToMany(targetEntity="Historic", mappedBy="user")
     */
    private $historic;

    /**
     * @ORM\Column(type="boolean")
     *
     */
    private $isActive;

    /**
     * @ORM\OneToMany(targetEntity="Petition", mappedBy="user")
     *
     */
    private $petitions;

    /**
     * @ORM\Column(type="text", options={"default": "en"})
     *
     */
    private $locale;

    /**
     * @ORM\OneToMany(targetEntity="Comments", mappedBy="event", cascade={"persist"})
     */
    private $comments;



    public function __construct()
    {
        $this->tags     = new \Doctrine\Common\Collections\ArrayCollection();
        $this->historic  = new \Doctrine\Common\Collections\ArrayCollection();
        $this->comments  = new \Doctrine\Common\Collections\ArrayCollection();
        $this->locale = 'en';
        $this->isActive = false;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param mixed $surname
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
    }


    /**
     * @param mixed $userName
     */
    public function setUserName($userName)
    {
        $this->userName = $userName;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    public function setPlainPassword($password)
    {
        $this->plainPassword = $password;
    }

    /**
     * @return mixed
     */
    public function getShipName()
    {
        return $this->shipName;
    }

    /**
     * @param mixed $shipName
     */
    public function setShipName($shipName)
    {
        $this->shipName = $shipName;
    }

    /**
     * @return mixed
     */
    public function getShipRank()
    {
        return $this->shipRank;
    }

    /**
     * @param mixed $shipRank
     */
    public function setShipRank($shipRank)
    {
        $this->shipRank = $shipRank;
    }

    /**
     * @return mixed
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @param mixed $tags
     */
    public function setTags($tags)
    {
        $this->tags = $tags;
    }

    /**
     * @return mixed
     */
    public function gethistoric()
    {
        return $this->historic;
    }

    /**
     * @param mixed $historic
     */
    public function sethistoric($historic)
    {
        $this->historic = $historic;
    }

    /**
     * @return mixed
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * @param mixed $isActive
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    }

    /**
     * @return mixed
     */
    public function getPetitions()
    {
        return $this->petitions;
    }

    /**
     * @param mixed $petitions
     */
    public function setPetitions($petitions)
    {
        $this->petitions = $petitions;
    }

    /**
     * Add tag
     *
     * @param \AppBundle\Entity\Tag $tag
     *
     * @return User
     */
    public function addTag(\AppBundle\Entity\Tag $tag)
    {
        $this->tags[] = $tag;

        return $this;
    }

    /**
     * Remove tag
     *
     * @param \AppBundle\Entity\Tag $tag
     */
    public function removeTag(\AppBundle\Entity\Tag $tag)
    {
        $this->tags->removeElement($tag);
    }

    /**
     * Add historic
     *
     * @param \AppBundle\Entity\Event $historic
     *
     * @return User
     */
    public function addhistoric(\AppBundle\Entity\Historic $historic)
    {
        $this->historic[] = $historic;

        return $this;
    }

    /**
     * Add a peiition
     *
     * @param Petition $petition
     * @return $this
     */
    public function addPetition(\AppBundle\Entity\Petition $petition)
    {
        $this->petition[] = $petition;

        return $this;
    }

    /**
     * Remove historic
     *
     * @param \AppBundle\Entity\Event $historic
     */
    public function removehistoric(\AppBundle\Entity\Event $historic)
    {
        $this->historic->removeElement($historic);
    }

    //implementaciones de la interface

    public function getSalt()
    {
        // you *may* need a real salt depending on your encoder
        // see section on salt below
        return null;
    }

    public function getRoles()
    {
        return array('ROLE_USER');
    }

    public function eraseCredentials()
    {
    }

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->userName,
            $this->password,
            $this->isActive,
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->userName,
            $this->password,
            $this->isActive,
            ) = unserialize($serialized);
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->userName;
    }

    public function isAccountNonExpired()
    {
        return true;
    }

    public function isAccountNonLocked()
    {
        return true;
    }

    public function isCredentialsNonExpired()
    {
        return true;
    }

    public function isEnabled()
    {
        return $this->isActive;
    }

    /**
     * @return mixed
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @param mixed $locale
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;
    }

    /**
     * @return mixed
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * @param mixed $comments
     */
    public function setComments($comments)
    {
        $this->comments = $comments;
    }

    /**
     * @param Comments $comment
     */
    public function addComment($comment)
    {
        $this->comments[] = $comment;
    }



}
