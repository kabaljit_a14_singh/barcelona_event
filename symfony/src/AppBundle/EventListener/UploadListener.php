<?php

/**
 * Created by PhpStorm.
 * User: Gurdeep
 * Date: 04/05/2016
 * Time: 22:28
 */
namespace AppBundle\EventListener;

use AppBundle\Entity\FileEvo;
use Doctrine\Common\Persistence\ObjectManager;
use Oneup\UploaderBundle\Event\PostPersistEvent;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Validator\Constraints\DateTime;

class UploadListener
{
    /**
     * @var ObjectManager
     */
    private $om;

    public function __construct(ObjectManager $om)
    {
        $this->om=$om;
    }

    public function onUpload(PostPersistEvent $event)
    {
         //session para guardar las id de los ficheros
        $request = $event->getRequest();
        $session = $request->getSession();

        $file= $request->files->get('qqfile');
        $f=new FileEvo();

        $valid_imagetypes = array("png", "jpg", "jpeg");
        $valid_doctype=array("pdf","doc", "txt");

        $timeStamp = new \DateTime();
        $name = $event->getFile()->getFileName();
        $path=$file->getPathName();
        $size=$file->getSize();
        $file_type = $file->guessClientExtension();
        
        if (in_array(strtolower($file_type), $valid_imagetypes)){
            $f->setType('image');
        }
        else if (in_array(strtolower($file_type), $valid_doctype)){
            $f->setType('document');
        }
        $f->setName($name);
        $f->setPath("/uploads/gallery/".$name);
        $f->setSize($size);
        $f->setEvent(null);
        $this->om->persist($f);

        $this->om->flush();

        if($session->get('filesIDs')){

            $filesIds= $session->get('filesIDs');
            $filesIds[]=$f->getId();
            $session->set('filesIDs',$filesIds);
            $countFiles=count($session->get('filesIDs'));
            $session->set('countFiles',$countFiles);
        }
        else {
            $filesIds[]=$f->getId();
            $session->set('filesIDs',$filesIds);
            $session->set('countFiles',1);
        }

    }
}

