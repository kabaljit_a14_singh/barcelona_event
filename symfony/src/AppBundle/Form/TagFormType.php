<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class TagFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'name',
            TextType::class,
            array(
                'label' => 'str_nombre',
                'required' => true,
                'constraints' => array(
                    new NotBlank(array('message' => 'str_obligatorio')),
                )
            )
        );

        $builder->add(
            'description',
            TextType::class,
            array(
                'label' => 'str_descripcion',
                'required' => false,
            )
        );

    }

    public function getName()
    {
        return 'tag';
    }

    public function setDefaultOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle/Entity/Tag',
            'csrf_protection'	 => true,
            'csrf_field_name'	 => '_token',
        ));
    }
}