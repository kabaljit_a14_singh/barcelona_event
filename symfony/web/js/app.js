$(function(){
    $('article.event').hover(
        // Efecto de entrada del mouse
        function(){
            $(this).find('div.event-image-wrapper').css('transform', 'scale(1.1)');
            $(this).find('img').css('transform', 'rotate(-5deg)');
        },
        // Efecto de salida del mouse
        function(){
            $(this).find('div.event-image-wrapper').css('transform', 'scale(1)');
            $(this).find('img').css('transform', 'rotate(0deg)');
        }
    );
});